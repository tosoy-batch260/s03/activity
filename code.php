<?php

class Person
{
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName)
    {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName()
    {
        echo "You full name is $this->firstName $this->lastName.";
    }
}

class Developer extends Person {
    public function printName()
    {
        echo "You name is $this->firstName $this->middleName $this->lastName and you are a developer.";
    }
}

class Engineer extends Person {
    public function printName()
    {
        echo "You are an engineer named $this->firstName $this->middleName $this->lastName.";
    }
}

$person = new Person('Marlon', 'Luceño', 'Morfe');
$developer = new Developer('Alexzandra', 'Flores', 'Fernando');
$engineer = new Engineer('Clint Alvin', 'Generale', 'Cesar');